<?php $__env->startSection('conteudo'); ?>
<?php echo e(csrf_field()); ?>  
<?php if(@$usuario == "admin"): ?>
<a type="button" href="<?php echo e('/admin'); ?>" class="btn btn-info">Incluir curso</a>    
<?php endif; ?>


<div class="clearfix">&nbsp;</div>
<h5 class="card-title btn-primary"><marquee>Projeto desenvolvido por tayrone.</marquee></h5>
<div class="container">
<div class="card bg-dark text-white">
  <img class="card-img" src="http://virginiacenter.com.br/wp-content/uploads/2016/07/Capa_FB_PromocoesdeJulho1.png" alt="Card image">
  <div class="card-img-overlay">
    <p class="card-text"></p>
    <p class="card-text"></p>
  </div>
</div>

<div class="clearfix">&nbsp;</div>
<div class="container">
            <div class="row">
              <?php $__currentLoopData = @$listarCursos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                  <div class="row-sm-3">
                    <div class="card border-secondary mb-3" style="max-width: 12rem;">
                      <div class="card-header"><?php echo e(@$row->nome); ?></div>
                        <div class="card-body text-secondary">
                        <img class="card-img" src="https://png.pngtree.com/element_origin_min_pic/17/03/26/852f2a73cf18c96fcecd8e464c69373d.jpg" alt="Card image">
                          <h5 class="card-title">_____________</h5>
                          <p class="card-text"><?php echo e(@$row->descricaoCurso); ?></p>
                          <button type="button" class="btn btn-primary" onclick="modalGlobalOpen('<?php echo e(route("modalCursos.modalCursos", ["id" => @$row->id, "admin" => @$usuario])); ?>')">Ver</button>
                          <?php if(@$row->precoCurso == null): ?> <h5 class="card-title">R$ 0</h5> <?php elseif(@$row->precoCurso != null): ?> <h5 class="card-title">R$ <?php echo e($row->precoCurso); ?></h5> <?php endif; ?>
                        </div>
                      </div>
                    </div>    
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
            </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>