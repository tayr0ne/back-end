## Backend
**Next Tech: desafio**

**Principais competências**:
* ( PHP - Symfony/Laravel)
* ( RESTful - json)
* ( Tests - Unitários/WebTestCase)
* Git ( Procedimentos básicos + Fork + PR )
* ( Linux || Mac ) + Familiaridade com linha de comando


## O Desafio

Um mesmo curso pode ter uma ou mais categoria.

Pense no CRUD de cada modelo, desenvolva endpoints para listagem de registros, criação de novo, detalhamento, edição e remoção.

Para operar os endpoints, uma autenticação deve ser enviada e validada junto a requisição de cada endpoint.

Os dados da API poderão ser mockados para facilitar a implementação.


## Considerações Gerais

Você deverá usar este repositório como o principal do projeto. Todos os seus commits devem estar registrados aqui.

**Registre tudo**: Ideias que gostaria de implementar se tivesse mais tempo (explique como você as resolveria), decisões tomadas e seus porquês, arquiteturas testadas e os motivos de terem sido modificadas ou abandonadas.

Sinta-se livre para incluir ferramentas e bibliotecas open source.

Avaliaremos sua submissão como se fosse um produto mínimo viável (MVP), pronto para ser publicado em produção, mas que continuará sendo expandido e mantido no futuro. A avaliação terá como foco as seguintes características:

* se atende aos requisitos básicos.
* a qualidade do código, do projeto de software, da arquitetura.
* os testes.
* a automação.
* a escalabilidade.

Em caso de dúvidas, pergunte.


## A aplicação

A aplicação possui dois modelos:

    - Category

    - Course


## Executando o desafio

Este repositório é privado e sem permissão de escrita. Você precisará fazer um fork dele para sua conta, e ao terminar, gerar um 'pull request' para avaliação. Quando encerrar, nos avise pelo contato do processo seletivo.


## Diferenciais

* Recursos da listagem (paginação, ordenação, filtro)

* Caso de teste para cada cenário.


## Observações

* Forneça uma documentação para que utilizemos os endpoints.

* Forneça também um README com documentação dos endpoints e que nos ajude a rodar sua aplicação localmente (ex.: tecnologias utilizadas, prérequisitos, dependências).

Sua aplicação também deve responder a requisições 'preflight'
